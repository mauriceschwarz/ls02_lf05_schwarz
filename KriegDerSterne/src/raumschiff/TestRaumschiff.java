package raumschiff;



public class TestRaumschiff {

	public static void main(String[] args) {
		//Raumschiffe
		//Raumschiff Klassenname = (Schiffsname, lebenserhaltung, huelle, schilde,energie, androiden, photonen)
		Raumschiff klingonen = new Raumschiff("IKS Hegh'ta", 100, 100, 100, 100 ,2, 1);
		Raumschiff romulaner = new Raumschiff("IRW Kharzara", 100, 100, 100, 100, 2, 2);
		Raumschiff vulkanier = new Raumschiff("Ni'war", 100, 50, 80, 80, 5 , 0 );
		
		//Ladung
		Ladung ferengiS = new Ladung("Ferengi Schneckensaft", 200);
		Ladung borgS = new Ladung("Borg Schrott",5);
		Ladung roteM = new Ladung("Rote Materie", 2);
		Ladung froschungsS= new Ladung("Forschungssonde", 35);
		Ladung batKS = new Ladung("Bat'leth Klingonen Schwert", 200);
		Ladung plasmaW  = new Ladung("Plasma-Waffe", 50);
		Ladung photonenT= new Ladung("Photonentorpedos", 3);
		

		//Beladen
		klingonen.addLadung(ferengiS);
		klingonen.addLadung(batKS);
		romulaner.addLadung(borgS);
		romulaner.addLadung(roteM);
		romulaner.addLadung(plasmaW);
		vulkanier.addLadung(froschungsS);
		vulkanier.addLadung(photonenT);
		
		//Funktionen 
		
		klingonen.photonentorpedosSchiessen(romulaner);
		romulaner.photonentorpedosSchiessen(klingonen);
		vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch");	
		klingonen.zustandRaumschiff();
		klingonen.ladungsverzeichnisAusgeben();
		vulkanier.reparaturDurchfuehren(true, true, true, 5);
		vulkanier.photonentorpedosLaden(3);
		vulkanier.ladungsverzeichnisAusgeben();
		klingonen.photonentorpedosSchiessen(romulaner);
		klingonen.photonentorpedosSchiessen(romulaner);
		vulkanier.ladungsverzeichnisAusgeben();
		romulaner.ladungsverzeichnisAusgeben();
		romulaner.zustandRaumschiff();
		vulkanier.zustandRaumschiff();
	
	}
	
	
}