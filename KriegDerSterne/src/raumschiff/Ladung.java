package raumschiff;
/**
 * 
 * @author maurice
 *
 */
public class Ladung {
	
	//------------------------------- Attribute -------------------------------

	private String bezeichnung;
	private int menge; 
	
	//------------------------------- getter and setter -------------------------------
	
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung=bezeichnung;
	}
	public String getBezeichnung() {
		return this.bezeichnung;
	}
	public int getMenge() {
		return menge;
	}
	public void setMenge(int menge) {
		this.menge = menge;
	}
	//------------------------------- constructor -------------------------------
	public Ladung () {
		this.bezeichnung="unbenannt";
		this.menge=0;
	}
	public Ladung(String bezeichnung, int menge) {
		setBezeichnung(bezeichnung);
		setMenge(menge);
	}
	
	
	
	
}
