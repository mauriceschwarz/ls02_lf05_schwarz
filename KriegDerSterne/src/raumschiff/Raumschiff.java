package raumschiff;
import java.lang.String;
import java.util.ArrayList;
import java.util.Random;

import raumschiff.Ladung;


/**
 * 
 * @author maurice
 *
 */
public class Raumschiff {
	
	
	//------------------------------- Attribute -------------------------------
	 
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeProzent;
	private int huelleProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private ArrayList<String> broadcastKommunikator = new ArrayList<>();
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
	
	//------------------------------- constructor -------------------------------
	public Raumschiff(){
		this.energieversorgungInProzent= 0;
		this.lebenserhaltungssystemeInProzent =0;
		this.huelleProzent=0;
		this.schildeProzent=0;
		this.androidenAnzahl=0;
		this.photonentorpedoAnzahl=0;
		this.schiffsname="Schiff";
		
	}
	public Raumschiff(String schiffsname, int lebenserhaltungssystemeInProzent , int huelleProzent,  int schildeProzent, int energieversorgungInProzent,int androidenAnzahl, int photonentorpedoAnzahl) {
		
		this.schiffsname=schiffsname;
		this.energieversorgungInProzent =energieversorgungInProzent;
		this.lebenserhaltungssystemeInProzent =lebenserhaltungssystemeInProzent;
		this.huelleProzent=huelleProzent;
		this.schildeProzent=schildeProzent;
		this.androidenAnzahl=androidenAnzahl;
		this.photonentorpedoAnzahl=photonentorpedoAnzahl;
	}
	
	
	//------------------------------- getter and setter -------------------------------
	
	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	
	}
	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	
	}
	
	public int getEnergieversorgungINProzent() {
		return energieversorgungInProzent;
	}
	
	public void setEnergieversorgungINProzent(int energieversorgungINProzent) {
		this.energieversorgungInProzent = energieversorgungINProzent;
	}
	
	public int getSchildeProzent() {
		return schildeProzent;
	}
	
	public void setSchildeProzent(int schildeProzent) {
		this.schildeProzent = schildeProzent;
	}
	
	public int getHuelleProzent() {
		return huelleProzent;
	}
	
	public void setHuelleProzent(int huelleProzent) {
		this.huelleProzent = huelleProzent;
	}
	
	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}
	
	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}
	
	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}
	
	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}
	
	public String getSchiffsname() {
		return schiffsname;
	}
	
	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}
	
	public ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}
	
	public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		this.broadcastKommunikator = broadcastKommunikator;
	}
	
	public ArrayList<Ladung> getLadungsverzeichnis() {
		return this.ladungsverzeichnis;
	}
	
	public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
		this.ladungsverzeichnis = ladungsverzeichnis;
	}
	
	
	//------------------------------- Methoden -------------------------------
	
	
	/**
	 * 
	 * Gibt alle Attribute der Klasse Raumschiff fomatiert aus.
	 */
	public void zustandRaumschiff() {
        System.out.println("Raumschiff Zustand:");
        System.out.println("Photonentorpedo Anzah: " + this.photonentorpedoAnzahl);
        System.out.println("Energieversorgung: " + this.energieversorgungInProzent + " %");
        System.out.println("Lebenserhaltungssysteme" + this.lebenserhaltungssystemeInProzent + "%");
        System.out.println("Schilde: " + this.schildeProzent + " %");
        System.out.println("Hülle: " + this.huelleProzent + " %");
        System.out.println("Androiden Anzahl: " + this.androidenAnzahl);
        System.out.println("");
        System.out.println("Ladungsverzeichnis: ");
        System.out.println("Bezeichnung: " + this.ladungsverzeichnis.get(0));
        System.out.println("Menge: " + this.ladungsverzeichnis.get(1));
    }
	
	/**
	 * Gibt die Menge und Bezeichnung der arrayList ladungsverzeichnis aus, die für den Inhalt der Klasse Ladung steht.
	 */
	public void ladungsverzeichnisAusgeben() {
		
		for(Ladung l: this.ladungsverzeichnis) {
			System.out.println("Bezeichnung: " + l.getBezeichnung());
			System.out.println("Menge: " + l.getMenge());
		}
		
	}
	/**
	 * Suche im Ladungsverzeichnis nach Elementen mit Menge Null
	 * Wenn ein Element die Menge Null besitz wird es aus dem Ladungsverzeichnis entfernt 
	 */
	public void reaumeLadungsverzeichnisAuf() {
		for(int i=0; i < this.ladungsverzeichnis.size(); i++) {
			if(this.ladungsverzeichnis.get(i).getMenge()==0) {
				ladungsverzeichnis.remove(i);
			}
		}
	}
	
	/**
	 * Der Funktion wird ein Ziel r für die Waffen übergeben
	 * Gibt das getroffene Raumschiff r aus
	 * @param r
	 */
	public void photonentorpedosSchiessen(Raumschiff r) {
		if(this.photonentorpedoAnzahl >= 1) {
			System.out.println("Photonentorpedos abgeschossen");
			this.photonentorpedoAnzahl=this.photonentorpedoAnzahl-1;
			treffer(r);
 
		} else {
			System.out.println("-=*Click*=-");
		}
	
	}
	/**
	 * Übergibt der phaserkanone ein Raumschiff r als zielt und feuert 
	 * Gibt das getroffene Raumschiff r aus 
	 * @param r
	 */
	public void phaserkanoneSchiessen(Raumschiff r) {
		if(this.energieversorgungInProzent > 50) {
			System.out.println("Phaserkanone abgeschossen");
			this.energieversorgungInProzent=this.energieversorgungInProzent-50;
			treffer(r);
		} else {
			System.out.println("-=*Click*=-");
		}
	}
	
	/**
	 * Gibt an welches Ziel durch die Waffensystem getroffen wurde
	 * @param r
	 */
	private void treffer(Raumschiff r) {
		System.out.println(r.getSchiffsname() + " wurde getroffen");
	}
	
	/**
	 * Fügt dem Ladungsverzeichnis neue Ladung hinzu
	 * @param neueLadung
	 */
	public void addLadung(Ladung neueLadung) {
		this.ladungsverzeichnis.add(neueLadung);
	}
	
	/**
	 * Vermerkt die Treffer am Raumschiff 
	 * gibt aus wenn die huelle des Schiffes == 0 und die Lebenserhaltungssystem nicht mehr funktionieren 
	 */
	public void vermerkeTreffer() {
		this.schildeProzent -= 50;
 
		if(this.schildeProzent <= 0) {
			this.huelleProzent -= 50;
			this.energieversorgungInProzent -= 50;
		}
 
		if(this.huelleProzent <= 0) {
			System.out.println("Die Lebenenserhaltungssysteme wurden vernichtet");
		}
	}
	/**
	 * 
	 * @return
	 */
	public ArrayList<String> eintraegeLogbuchZurueckgeben() {
		return this.broadcastKommunikator;
	}
	/**
	 * Fügt eine Nachricht dem broadcastKommunikator hinzu und gibt diese in der Konsole aus
	 * @param message
	 */
	public void nachrichtAnAlle(String message) {
		this.broadcastKommunikator.add(message);
		System.out.println(message);
	}
	/**
	 * Lädt Photonentorpedos aus der Ladung in das Raumschiff
	 * Fügt also eine Menge x dem Schiff hinzu und entfernt die gleiche Menge aus dem laddungsverzeichnis 
	 * Sollten die Menge x größer sein als die Menge die im Ladungsverzeichnis vorhanden ist, wird nur die Menge verwendet bis == 0
	 * @param anzahlTorpedos
	 */
	public void photonentorpedosLaden(int anzahlTorpedos) {
		if(this.photonentorpedoAnzahl <= 0) {
			System.out.println("Keine Photonentorpedos gefunden");
			System.out.println("-=*Click*=-");
		}
		if(anzahlTorpedos > this.photonentorpedoAnzahl) {
			this.photonentorpedoAnzahl= anzahlTorpedos;
			
			for(Ladung l: this.ladungsverzeichnis) {
				if(l.getBezeichnung().equalsIgnoreCase("Photonentorpedos")) {
					l.setMenge(anzahlTorpedos - this.photonentorpedoAnzahl);
					
					System.out.println("["+ anzahlTorpedos +"] Photonentorpedos eingesetzt ");
				}
			}
			
		}
	}
	/**
	 * Auftrag zum Repariren des Raumschiffes  
	 * @param schutzschilde
	 * @param energieversorgung
	 * @param schiffshuelle
	 * @param anzahlDroiden
	 */
	public void reparaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle, int anzahlDroiden) {
		int strukturen = 0;
		
		Random r = new Random();
		
		boolean schutzSchild = r.nextBoolean();
		if(schutzSchild == true) {
			strukturen += 1;
		}
		
		boolean energieVersorgung = r.nextBoolean();
		if(energieVersorgung == true) {
			strukturen += 1;
		}
		
		boolean schiffsHuelle = r.nextBoolean();
		if(schiffsHuelle == true) {
			strukturen += 1;
		}
		
		int value = r.nextInt(101);
		
		if(anzahlDroiden > this.androidenAnzahl) {
			this.androidenAnzahl = anzahlDroiden;
		}
		
		double shipStructures = ((value * (anzahlDroiden =- anzahlDroiden)) / (strukturen));
	}
	
	
	
}


